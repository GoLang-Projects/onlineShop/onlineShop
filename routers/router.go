package routers

import (
	"gitlab.com/Mar-er/onlineShop/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.UserController{})

	// namespace 路由
	ns := beego.NewNamespace("/api", 
		beego.NSNamespace("/user",
			beego.NSRouter("/register", &controllers.UserController{}, "POST:UserRegister"),
			beego.NSRouter("/login", &controllers.UserController{}, "POST:UserLogin"),
		),
		beego.NSNamespace("/good",
			beego.NSNamespace("/:good_id([0-9]+)",
				// http://localhost:8080/api/good/1/like?status=like
				beego.NSRouter("/like", &controllers.GoodController{}, "PUT:GoodLike"),
				beego.NSRouter("/comments", &controllers.GoodController{}, "GET:GetComments;POST:AddComment"),
			),
		),
	)
	// 注册 namespace
	beego.AddNamespace(ns)
}
