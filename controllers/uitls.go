package controllers

import (
	"fmt"
	"gitlab.com/Mar-er/onlineShop/models"
	"github.com/astaxie/beego/orm"
)

// 通过用户名查询用户信息
func GetUserByUsername(username string) *models.User {
	o := orm.NewOrm()
	user := models.User{Username: username}

	if err := o.Read(&user, "Username"); err == nil {
		return &user
	}else{
		return nil
	}
}


// 验证密码是否正确
func CheckoutPassword(user *models.User, OldPassword string) int {
	fmt.Println(user, OldPassword)
	if user.Password == OldPassword {
		return 0
	}
	return 1
}

// 登陆
func UserLogin(username string, password string) (int, *models.User){
	if user := GetUserByUsername(username); user == nil {
		return 2, nil
	}else{
		return CheckoutPassword(user, password), user
	}
}

// 点赞商品
func LikeGood(good_id int, like_status string) (bool, error){
	// 文档注释
	// 更新数据库的时候一定注意并发更新会导致数据出错,在完成更新数据的业务逻辑时，一定要时刻提醒自己并发更新的问题
	// orm.ColMinus和orm.ColAdd,使用这两行语句的时候，数据库会对该条数据加锁，
	// 每次update时都是查询数据库中的值然后增加1，而不是直接update good set follows=xxx where id = good_id 这种写法，因为xxx这个值可能是一个过期的值
	
	// beego官网说明 https://beego.me/docs/mvc/model/query.md
	// orm.ColValue 支持以下操作
		// ColAdd      // 加
		// ColMinus    // 减
		// ColMultiply // 乘
		// ColExcept   // 除
	o := orm.NewOrm()
	filter_ := orm.ColAdd
	fmt.Println(filter_, 54)
	if like_status == "unlike" {
		filter_ = orm.ColMinus
	}

	if _, err := o.QueryTable("good_detail").Filter("Id", good_id).Update(orm.Params{
			"follows": orm.ColValue(filter_, 1),
		}); err == nil {
        fmt.Println("关注成功")
        return true, nil
    } else {
        fmt.Println("\n\n关注失败: ", err.Error())
        return false, err
    }
}










