package controllers

import (
	"encoding/json"
	"github.com/astaxie/beego"
	"gitlab.com/Mar-er/onlineShop/models"
	"github.com/astaxie/beego/orm"
	"fmt"
	"gitlab.com/Mar-er/onlineShop/requests"
	"gitlab.com/Mar-er/onlineShop/response"
)

type UserController struct {
	beego.Controller
}

// func (c *UserController) Get() {
// 	c.TplName = "index.html"
// }

// 注册
func (c *UserController) UserRegister(){
	defer c.ServeJSON()
	username := c.GetString("username")
	password := c.GetString("password")
	o := orm.NewOrm()
	user := models.User{ Username:username, Password: password }

	// 判断该用户是否存在
	if o.QueryTable("User").Filter("Username", username).Exist() {
		c.Data["json"] = response.FailRes("用户名已被使用，请重新输入")
		return
	}else{
		_, err := o.Insert(&user)
		if err != nil {
			fmt.Printf("err: ", err.Error)
		}
		c.Data["json"] =  response.SuccRes("注册成功")
	}
}

// 登陆
func (c *UserController) UserLogin(){
	defer c.ServeJSON()
	var body requests.User

	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &body); err != nil {
		c.Data["json"] = response.FailRes("数据格式不正确")
	}else{
		username, password := body.Username, body.Password
		status, user := UserLogin(username, password);
		// 0 登录成功
		// 1 密码错误
		// 2 暂未注册 
		if status == 0 {
			// 登录成功，写入用户User.Id 到session中
			c.SetSession("session", user.Id)
			c.Data["json"] = map[string]interface{}{
				"ok": true,
				"data": map[string]interface{}{
					"username": user.Username,
					"nickname": user.Nickname,
					"id": user.Id,
					"email": user.Email,
					"image": user.Image,
					"shop_cart": user.ShopCart,
					"history": user.History,
				},
			}
		}else if(status == 1){
			c.Data["json"] = response.FailRes("用户名或者密码错误")	
		}else {
			c.Data["json"] = response.FailRes("改账号暂未注册")
		}
	}
}
