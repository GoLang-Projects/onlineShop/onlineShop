package controllers

import (
	"github.com/astaxie/beego"
	"fmt"
	"strconv"
	"gitlab.com/Mar-er/onlineShop/response"
)

type GoodController struct {
	beego.Controller
}

// 商品点赞
func (c *GoodController) GoodLike(){
	defer func() {
		fmt.Println(c.Data["json"])
		c.ServeJSON()
	}()

	// URL链接正则表达式匹配的参数
	good_id := c.Ctx.Input.Param(":good_id")
	// 链接后面的参数
	like_status := c.GetString("status")
	num, err := strconv.Atoi(good_id)

	if err != nil {
		c.Data["json"] = response.FailRes("good_id只接受int类型数据")
		return
	}
	
	if like_status != "like" && like_status != "unlike" {
		c.Data["json"] = response.FailRes("status值只能是like或unlike")
		return
	}

	// 当good_id, status参数都正确时才将数据保存至数据库
	LikeGood(num, like_status)
	var message string
	if like_status == "like" {
		message = "点赞成功"
	} else {
		message = "取消点赞成功"
	}
	c.Data["json"] = response.SuccRes(message)

	// 文档是这么写的，但是这也写错误提示不够友好，没有提示具体的错误信息
	// if err != nil || (like_status != "like" && like_status != "unlike") {
	// 	c.Data["json"] = response.FailRes("无效的good_id")
	// } else {
	// 	LikeGood(num, like_status)
	// 	var message string
	// 	if like_status == "like" {
	// 		message = "点赞成功"
	// 	} else {
	// 		message = "取消点赞成功"
	// 	}
	// 	c.Data["json"] = response.SuccRes(message)
	// }
}

// 商品评价
func (c *GoodController) AddComment() {
	
}






