package main

import (
    "fmt"
    "encoding/json"
)

func main() {
    type body struct {
        Key string
        Key2 string
        Key3 string
    }
    fmt.Println("")
    input := "{\"Key\": \"value\", \"Key2\": \"value2\"}"
    var dest body;
    if err := json.Unmarshal([]byte(input), &dest); err != nil {
        fmt.Println(err.Error())
    }
    fmt.Println("result", dest)
}
