package requests

type Success struct {
	Ok bool
}

type ErrorMessage struct {
	Ok bool
	ErrorMessage string
}

type User struct {
	Username string `json:"username"`  //告诉orm将字段首字母改为小写
	Password string `json:"password"`
}

